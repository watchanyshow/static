const { src, dest, parallel, series } = require('gulp');
const imagemin = require('gulp-imagemin');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const del = require("del");
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
// const uncss = require('gulp-uncss');

function images() {
    return src( "src/img/**" )
        .pipe(imagemin())
        .pipe(dest( "public/img" ))
}

function coronaImages() {
    return src( "src/corona/*" )
        .pipe(imagemin())
        .pipe(dest( "public/corona" ))
}

function clearPublic() {
    return del("public/**", {force:true})
}



function css() {
    return src( "src/css/*" )
        .pipe(cleanCSS())
        .pipe(rename({extname : ".min.css"}))
        .pipe(dest( "public/css"))
}

function js() {
    return src("src/js/*")
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(rename({extname:".min.js"}))
        .pipe(dest("public/js"))
}

function copyResources() {
    return src("src/resources/*")
        .pipe(dest("public/resources"))
}

function webCSS() {
    return src( "src/website/css/*.css" )
        .pipe(cleanCSS())
        .pipe(concat("websitev2.min.css"))
        // .pipe(uncss({
        //     html: ['http://localhost:3333']
        // }))
        .pipe(dest("public/css"));
}

function webJS() {
    return src( "src/website/js/*.js")
        .pipe(babel({
            presets: ['@babel/preset-env'],
            "comments": false,
            "compact": true
        }))
        .pipe(concat('websitev2.0.js'))
        // .pipe(dest('public/js/'))
        .pipe(uglify())
        // .pipe(rename({extname:".min.js"}))
        .pipe(dest('public/js/'));
}

function webFonts() {
    return src("src/website/fonts/**")
    .pipe(dest("public/fonts"));
}

function copy404() {
    return src("src/404.html")
    .pipe(dest("public"));
}

exports['images:img'] = images;
exports['images:corona'] = coronaImages;
exports.images = series(images, coronaImages);
exports['web:js'] = webJS;
exports['web:css'] = webCSS;
exports['web:fonts'] = webFonts;
exports['web'] = series(webJS, webCSS, webFonts);
exports.clearPublic = clearPublic;
exports.css = css;
exports.js = js;
exports.copyResources = copyResources;
exports.copy404 = copy404;
exports.build = series(clearPublic, parallel(exports.images, css, js, copyResources), exports.web, copy404);