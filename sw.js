var CACHE_STATIC_NAME = 'excess-static-v1.2.74'
var CACHE_DYNAMIC_NAME = 'excess-dynamic-v1' 
self.addEventListener('install', function(event) {
    console.log('[Service Worker] Installing Service Worker ...', event);
});

self.addEventListener('activate', function(event) {
    console.log('[Service Worker] Activating Service Worker ....', event);
    
    event.waitUntil(caches.open(CACHE_STATIC_NAME).then(function(cache) {
        console.log('[Service Worker] Precaching App Shell');
        
        // return ; //dont cache anything for now

    }));

    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
    var url = new URL(event.request.url, "https://watchany.tech");
    var request = event.request;
    var f;

    if(url.hostname.match(/\.herokuapp\.com/i) && url.searchParams.get("url").match(/googleapis\.com/i) && url.searchParams.get("headers[Authorization]")) {
        var _request = {
            method : request.method,
            credentials : request.credentials,
            cache : request.cache,
            destination : request.destination,
            redirect : request.redirect,
            referrer : request.referrer,
            mode : request.mode,
            headers : {},
        };

        console.log(_request);
        // request = new Request(url.searchParams.get("url"), _request);

        _request.headers["Authorization"] = url.searchParams.get("headers[Authorization]");
        console.log(_request);
        f = fetch(url.searchParams.get("url"), _request);
    }
    else {
        f = fetch(request);
    }
    
    console.log('SW: fetch', request);

    return event.respondWith(f);
});