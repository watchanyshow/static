window.addEventListener('load', function () {
    const form = document.getElementById("playWebForm");
    const inp = document.getElementById("playWebForm__code");

    form.addEventListener("submit", e => {
        e.preventDefault();

        window.location.href = `/p/${inp.value}`;
        return false;
    });

});